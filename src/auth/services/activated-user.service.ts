import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';
import { ConfigurationService } from '../../shared/services/configuration.service';

@Injectable()
export class ActivatedUser {

    public user: BehaviorSubject<any> = new BehaviorSubject(null);

    constructor(private configurationService: ConfigurationService, private userService: UserService) {
        this.configurationService.loaded.subscribe(() => {
            this.user.next(this.activatedUser);
        });
    }

    private get activatedUser() {
        return this.userService.getUserSync();
    }

}
