export declare interface LocationPermissionAccount {
  name: string;
}

export declare interface LocationPermissionTarget {
  name?: string;
  url: string;
  pattern?: RegExp;
}

export declare interface LocationPermission {
  redirectTo: any;
  privilege: string;
  account?: LocationPermissionAccount;
  target: LocationPermissionTarget;
  category?: string;
  mask?: number;
}
