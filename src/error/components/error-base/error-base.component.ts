import {Component, OnDestroy, OnInit, Renderer2, ViewEncapsulation} from '@angular/core';
import { ErrorService } from '../../error.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'universis-error-base',
  templateUrl: './error-base.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./error-base.component.scss']
})
export class ErrorBaseComponent implements OnInit, OnDestroy {


  public isUserLoggedIn = false;
  public code: string;
  public title: string;
  public message: string;
  public continue: string;
  public action: string;
  public continueLinkIsAbsolute: boolean;

  constructor(protected _errorService: ErrorService,
            protected _translateService: TranslateService,
              protected _renderer: Renderer2) {
      // hide universis-spinner
      const spinnerElement = document.body.getElementsByTagName('universis-spinner')[0];
      if (spinnerElement) {
        this._renderer.setStyle(spinnerElement, 'display', 'none');
      }
      // user logged in
      this.isUserLoggedIn = (sessionStorage.getItem('currentUser') != null);
      // add center content classes
      this._renderer.addClass(document.body, 'flex-row');
      this._renderer.addClass(document.body, 'align-items-center');
    // get last error
    const error = this._errorService.getLastError();
    // check error.code property
    if (error && typeof error.code === 'string') {
      this.code = error.code;
    } else if (error && typeof (error.status || error.statusCode) === 'number') {
      this.code = `E${error.status || error.statusCode}`;
    } else {
      this.code = 'E500';
    }
    if (error && typeof error.continue === 'string') {
      this.continue = error.continue;
    } else {
      this.continue = '/';
    }
    // check if continue link is an absolute URL
    this.continueLinkIsAbsolute = this.continueIsAbsolute();
    this.action = 'Error.Continue';

  }

    get displayCode() {
        if (/^E(\d+)\.?(\d+)?$/g.test(this.code)) {
            return this.code;
        }
        return 'E500';
    }

  ngOnInit() {


    this._translateService.get(this.code).subscribe((translation) => {
      if (translation) {
        this.title = translation.title;
        this.message = translation.message;
      } else {
        this._translateService.get('E500').subscribe((result) => {
          this.title = result.title;
          this.message = result.message;
        });
      }
    });
  }

    continueIsAbsolute(): boolean {
      const regex = new RegExp('^((https?|ftps?):\\/\\/)([\\da-z.-]+)\\.([a-z.]{2,6})([\\/\\w .-]*)*\\/?$');
      // returns true if the continue link is absolute
      return regex.test(this.continue);
    }

    ngOnDestroy(): void {
      // remove center content classes
        this._renderer.removeClass(document.body, 'flex-row');
        this._renderer.removeClass(document.body, 'align-items-center');
    }
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: '.m-error.m-error--http',
  templateUrl: './error-base.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./error-base.component.scss']
})
export class HttpErrorComponent extends ErrorBaseComponent implements OnInit, OnDestroy {
  constructor(protected _errorService: ErrorService,
    protected _translateService: TranslateService,
    private _router: Router,
    private _route: ActivatedRoute,
              protected _renderer: Renderer2) {
    super(_errorService, _translateService, _renderer);
  }

  ngOnInit() {
    // get params and query params
    this._route.params.subscribe((params) => {
      if (params.status) {
        this.code = `E${params.status}`;
      }
      this._route.queryParams.subscribe((queryParams) => {
        if (queryParams.continue) {
          this.continue = queryParams.continue;
          // check if continue link is an absolute URL
          this.continueLinkIsAbsolute = this.continueIsAbsolute();
        }
        if (queryParams.action) {
          this.action = queryParams.action;
        }
        this._translateService.get(this.code || 'E500').subscribe((translation) => {
          if (translation) {
            this.title = translation.title;
            this.message = translation.message;
          }
        });
      });
    });
  }
}
