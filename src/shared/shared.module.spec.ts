import {TestBed, async, inject} from '@angular/core/testing';
import {SharedModule} from './shared.module';
import {AngularDataContext, MostModule} from '@themost/angular';
import {UserStorageService} from './services/user-storage';

describe('SharedModule', () => {
  beforeEach(async(() => {
    // noinspection JSIgnoredPromiseFromCall
    TestBed.configureTestingModule({
      imports: [
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        SharedModule.forRoot()
      ],
      declarations: [
      ],
    }).compileComponents();
  }));

  it('should inject services', inject([UserStorageService], (userStorageService: UserStorageService) => {
      expect(userStorageService).toBeTruthy();
  }));

});
