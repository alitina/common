import {async, ComponentFixture, TestBed,} from '@angular/core/testing';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {ModalModule} from 'ngx-bootstrap';
import { SignatureInfoComponent } from './signature-info.component';
import {SharedModule} from '../../shared.module';
import {CertificateService} from '../../services/certificate-service/certificate-service.service';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {mockCert} from '../../services/certificate-service/mock-certificate';
import {By} from '@angular/platform-browser';

describe('SignatureInfoComponent', () => {
  let component: SignatureInfoComponent;
  let fixture: ComponentFixture<SignatureInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        ModalModule.forRoot(),
        RouterTestingModule,
        SharedModule.forRoot(),
        TranslateModule.forRoot()
      ],
      providers: [
        CertificateService,
        BsModalRef,
        BsModalService,
        DatePipe,
        TranslateService
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignatureInfoComponent);
    component = fixture.componentInstance;
    component.certificate = mockCert.cert;
    component.signatureBlock = mockCert.signature;
    component.dateCreated = new Date();
    component.examinationCode = "ABC123"
    component.isVerified = true;
  });

  it('should create', () => {
    component.certificate = mockCert.cert;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should display key purposes', () => {
    fixture.detectChanges();
    expect(component.tab).toEqual(1);
    const element = fixture.debugElement.queryAll(By.css('.card-body'));
    expect(element.length).toBe(2);
    mockCert.purposes.forEach(x => {
      expect(element[1].nativeElement.textContent).toContain(x);
    });
  });
  it('should display the validity date range', () => {
    const datePipe = TestBed.get(DatePipe);
    fixture.detectChanges();
    expect(component.tab).toEqual(1);
    const elements = fixture.debugElement.queryAll(By.css('.row.font-lg'));
    expect(elements.length).toEqual(3);
    expect(elements[2].nativeElement.textContent).toContain(datePipe.transform(mockCert.fromDate, 'shortDate'));
    expect(elements[2].nativeElement.textContent).toContain(datePipe.transform(mockCert.toDate, 'shortDate'));
  });
  it('should display the modal title', () => {
    fixture.detectChanges();
    const title = fixture.debugElement.query(By.css('.modal-body.mt-0.pt-0'));
    expect(component.user).toBeTruthy();
    expect(component.user.givenName).toEqual("Universis");
    expect(component.user.familyName).toEqual("");
    expect(title.nativeElement.textContent).toContain(component.user.givenName);
    expect(title.nativeElement.textContent).toContain(component.examinationCode);
  });

  it('should display certificate details', () => {
    component.tab =2
    fixture.detectChanges();
    const groupList = fixture.debugElement.query(By.css('#group-list'));
    expect(groupList.nativeElement.textContent).toContain(mockCert.signature);
    expect(groupList.nativeElement.textContent).toContain(mockCert.version);
    expect(groupList.nativeElement.textContent).toContain(mockCert.algorithm);
    expect(groupList.nativeElement.textContent).toContain("CN=Universis");
    expect(groupList.nativeElement.textContent).toContain(mockCert.serialNumber);
  });

  it('should return attributes array', () => {
    const certSvc: CertificateService = TestBed.get(CertificateService)
    const attributes = component.resolveAttributes(certSvc.getX509Certificate(mockCert.cert));
    expect(Array.isArray(attributes)).toBeTruthy();
    expect(attributes.filter(x=> x.translationKey.includes('Version'))[0].value).toEqual(mockCert.version);
    expect(attributes.filter(x=> x.translationKey.includes('SerialNumber'))[0].value).toEqual(mockCert.serialNumber);
    expect(attributes.filter(x=> x.translationKey.includes('Algorithm'))[0].value).toEqual(mockCert.algorithm);
    expect(attributes.filter(x=> x.translationKey.includes('Issuer'))[0].value).toContain("CN=Universis CA");
    expect(attributes.filter(x=> x.translationKey.includes('Subject'))[0].value).toContain("CN=Universis");
  });
  it('should return general attributes array', () => {
    const certSvc: CertificateService = TestBed.get(CertificateService);
    const attributes = component.getGeneralAttributes(certSvc.getX509Certificate(mockCert.cert));
    expect(Array.isArray(attributes)).toBeTruthy();
    expect(attributes.filter(x=> x.translationKey.includes('IssuedTo'))[0].value).toEqual("Universis");
    expect(attributes.filter(x=> x.translationKey.includes('Issuer'))[0].value).toEqual("Universis CA");
    expect(attributes.filter(x=> x.translationKey.includes('NotValidBeforeShort'))[0].value).toContain(new Date(mockCert.fromDate).toString());
    expect(attributes.filter(x=> x.translationKey.includes('NotValidAfterShort'))[0].value).toContain(new Date(mockCert.toDate).toString());
  });

});
