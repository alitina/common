export const el = {
  SignatureInfo: {
    CheckHash: "Hash επαλήθευσης",
    SignatureBlock: "Τμήμα υπογραφής",
    Issuer: "Εκδότης",
    Subject: "Θέμα",
    SerialNumber: "Σειριακός αριθμός",
    Version: "Εκδοση",
    NotValidBefore: "Ημερομηνία έναρξης ισχύος",
    NotValidAfter: "Ημερομηνία λήξης",
    Algorithm: "Αλγόριθμος",
    PublicKey: "Δημόσιο κλειδί",
    PublicKeyParameters: "Παράμετροι δημόσιου κλειδιού",
    Field: "Πεδίο",
    Value: "Τιμή",
    General: "Γενικά",
    Details: "Λεπτομέρειες",
    CertificateInformation: "Πληροφορίες πιστοποιητικού",
    IssuedTo: "Εκδόθηκε για",
    NotValidBeforeShort: "Ισχύει από",
    NotValidAfterShort: "έως",
    Verified: "Επαληθευμένη",
    NotVerified: "Μη επαληθευμένη",
    Close : "Κλείσιμο",
    CertificatePurposes: {
      Header: "Αυτο το πιστοποιητικό μπορεί να χρησιμοποιηθεί για τους παρακάτω σκοπούς:",
      emailProtection: "Προστασία μήνυμα ηλεκτρονικού ταχυδρομείου",
      documentSign: "Υπογραφή εγγράφων",
      clientAuth: "Επαλήθευση της ταυτότητά σας σε έναν απομακρυσμένο υπολογιστή",
      smartCardLogon: "Να σας επιτρέψει να συνδέεστε σε συσκευές με τη χρήση έξυπνης κάρτας",
      nonRepudiation: "Μη αποποίηση ευθύνης",
      digitalSignature: "Ψηφιακή Υπογραφή",
      documentEnc: "Κρυπτογράφηση εγγράφου",
      serverAuth: "Επαλήθευση της ταυτότητας του διακομιστή σας",
      codeSigning: "Υπογραφή κώδικα",
      timestamping: "Χρονική Σήμανση",
      keyEncipherment: "Ως κλειδί κρυπτογράφησης",
      any: "Οποιοδήποτε άλλο σκοπό"
    }
  }
}
