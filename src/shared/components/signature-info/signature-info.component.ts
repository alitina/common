import { Component, Input, OnInit } from '@angular/core';
import { X509 } from 'jsrsasign';
import { CertificateService } from '../../services/certificate-service/certificate-service.service';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../../../environments/environment';
import {SIGNATURE_INFO} from './i18n/signature-info.translations';

/**
 *
 * @interface SignatureAttribute
 *
 * @property {string} translationKey The translation key to show to the user
 * @property {string} value The value to show to the user
 * @property {object} Validation data. isValid is the validation status, showValidation indicates whether to show the validation
 *
 */
interface SignatureAttribute {
  translationKey: string;
  value: string;
  validation?: {
    isValid: boolean;
    showValidation: boolean;
  }
}

@Component({
  selector: 'universis-signature-info',
  templateUrl: './signature-info.component.html'
})
export class SignatureInfoComponent implements OnInit {

  /**
   * @property {Object} user The user who submitted the exam  document
   */
  @Input() user;

  /**
   * @property {boolean} isVerified A flag to indicate whether the signature was
   * verified at least once on server side. Can be use to show to the user that
   * the signature was accepted at some point in the past even if the certificate is expired.
   */
  @Input() isVerified: boolean;
  @Input() certificate: string;
  @Input() signatureBlock: string;
  @Input() checkHashKey: string;
  @Input() examinationCode: string;
  @Input() dateCreated: Date;


  /**
   * @property The list of attributes to show to the user
   */
  public signatureAttributes: SignatureAttribute[] = [];
  public tab: number  = 1;
  public generalAttributes: SignatureAttribute[] = [];
  public purposes: Array<any>;

  constructor(private _bsModalRef: BsModalRef,
              private _certificateService: CertificateService,
              private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      if(SIGNATURE_INFO[culture])
        this._translateService.setTranslation(culture, SIGNATURE_INFO[culture], true);
    });
  }

  async ngOnInit() {
    this.signatureAttributes = [
      {
        translationKey: 'SignatureInfo.CheckHash',
        value: this.checkHashKey
      },
      {
        translationKey: 'SignatureInfo.SignatureBlock',
        value: this.signatureBlock
      }
    ];
    try {
      let parsed;
      parsed = this._certificateService.getX509Certificate(this.certificate);
      if(!this.user){
        this.user = this._certificateService.extractCertificateOwner(parsed);
      }
      const certAttributes = this.resolveAttributes(parsed);
      this.purposes = this._certificateService.extractPurposes(parsed);
      this.signatureAttributes = [...certAttributes, ...this.signatureAttributes];
      this.generalAttributes = this.getGeneralAttributes(parsed);
    } catch(err) {
      console.log('err: ', err);
    }
  }

  /**
   *
   * Resolves a certificate to usable fields
   *
   * @param {X509} certificate The X509 object certificate
   *
   */
  resolveAttributes(certificate: X509) {
    const certificateAttributes = [];
    certificateAttributes.push({
      translationKey: 'SignatureInfo.Version',
      value: certificate.getVersion()
    });
    certificateAttributes.push({
      translationKey: 'SignatureInfo.SerialNumber',
      value: certificate.getSerialNumberHex()
    });
    certificateAttributes.push({
      translationKey: 'SignatureInfo.Algorithm',
      value: certificate.getSignatureAlgorithmField()
    });
    certificateAttributes.push({
      translationKey: 'SignatureInfo.Issuer',
      value: (certificate.getIssuer().str).replace(/\//g, '\n')
    });
    certificateAttributes.push({
      translationKey: 'SignatureInfo.NotValidBefore',
      value: this._certificateService.parseCertificateDate(
        certificate.getNotBefore()
      ),
      validation: {
        isValid: new Date() >= this._certificateService.parseCertificateDate(
          certificate.getNotBefore()
        ),
        showValidation: true
      }
    });
    certificateAttributes.push({
      translationKey: 'SignatureInfo.NotValidAfter',
      value:  this._certificateService.parseCertificateDate(
        certificate.getNotAfter()
      ),
      validation: {
        isValid: this._certificateService.parseCertificateDate(
          certificate.getNotAfter()
        ) >= new Date(),
        showValidation: true
      }
    });
    certificateAttributes.push({
      translationKey: 'SignatureInfo.Subject',
      value: (certificate.getSubject().str).replace(/\//g, '\n')
    });
    certificateAttributes.push({
      translationKey: 'SignatureInfo.PublicKey',
      value: certificate.getPublicKeyHex()
    });
    certificateAttributes.push({
      translationKey: 'SignatureInfo.PublicKeyParameters',
      value: certificate.getPublicKeyIdx()
    });
    return certificateAttributes;
  }

  getGeneralAttributes(certificate): SignatureAttribute[] {
    let attributes = [];
    attributes.push({
      translationKey: 'SignatureInfo.IssuedTo',
      // get certificate subject common name
      value: (certificate.getSubjectString()).split('/').filter(x => x.includes('CN')).join(',').split('=')[1]
    });
    attributes.push({
      translationKey: 'SignatureInfo.Issuer',
      // get certificate issuer common name (CN)
      value: (certificate.getIssuerString()).split('/').filter(x => x.includes('CN')).join(',').split('=')[1]
    });
    attributes.push({
      translationKey: 'SignatureInfo.NotValidBeforeShort',
      value: this._certificateService.parseCertificateDate(
        certificate.getNotBefore()
      ).toString(),
      validation: {
        isValid: new Date() >= this._certificateService.parseCertificateDate(
          certificate.getNotBefore()
        ),
        showValidation: true
      }
    });
    attributes.push({
      translationKey: 'SignatureInfo.NotValidAfterShort',
      value:  this._certificateService.parseCertificateDate(
        certificate.getNotAfter()
      ).toString(),
      validation: {
        isValid: this._certificateService.parseCertificateDate(
          certificate.getNotAfter()
        ) >= new Date(),
        showValidation: true
      }
    });
    return attributes;
  }

  hideCertificateModal() {
    this._bsModalRef.hide();
  }
}
