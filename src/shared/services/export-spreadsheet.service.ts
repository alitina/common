import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

@Injectable()
  export class ExportSpreadsheetService {

    private saveAs(buffer: any, file: string): void {
        const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
        FileSaver.saveAs(data, file);
    }

    export(data: any[], file: string, options?: {
      header?: string[];
      skipHeader?: boolean;
  }) {
        const worksheet = XLSX.utils.json_to_sheet(data, {
          skipHeader: !!(options && options.skipHeader)
        });
        if (options.header && !options.skipHeader) {
          options.header.forEach((header, index) => {
            const cell = worksheet[XLSX.utils.encode_col(index) + '1'];
            cell.v = header;
          });
        }
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAs(excelBuffer, file);
    }
  }
