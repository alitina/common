import {LocalUserStorageService, SessionUserStorageService} from './browser-storage.service';
import {AngularDataContext, MostModule} from '@themost/angular';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ApiTestingController} from '../../../testing/src/api-testing-controller.service';
import {ApiTestingModule} from '../../../testing/src/api-testing.module';
import {TestBed, inject, async} from '@angular/core/testing';
import * as _ from 'lodash';

describe('LocalUserStorageService', () => {
  let mockApi: ApiTestingController;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        ApiTestingModule.forRoot(),
        CommonModule,
        FormsModule,
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [
      ],
      providers: [
        LocalUserStorageService
      ]
    }).compileComponents();
    mockApi = TestBed.get(ApiTestingController);
  });

  afterAll(async() => {
    localStorage.clear();
  });

  it('should initiate a new service', inject([AngularDataContext], (context: AngularDataContext) => {
    const localStorageService = new LocalUserStorageService(context);
    expect(localStorageService).toBeTruthy();
  }));

  it('should get item from localStorage', inject( [LocalUserStorageService],
    async(localStorageService: LocalUserStorageService) => {
    expect(await localStorageService.getItem('key')).toEqual({key: 'key', value: null});
  }));

  it('should set item to localStorage', inject( [LocalUserStorageService],
    async(localStorageService: LocalUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await localStorageService.setItem(key, value);
    const localStorageItem = JSON.parse(localStorage.getItem('userLocalStorage'));
    const result = _.has(localStorageItem, key);
    const expectedValue = _.get(localStorageItem, key);
    expect(result).toBe(true);
    expect(expectedValue).toEqual(value);
  }));
  it('should return remove an item from localStorage', inject([LocalUserStorageService],
    async(localStorageService: LocalUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await localStorageService.setItem(key, value);
    await localStorageService.removeItem(key);
    const localStorageItem = JSON.parse(localStorage.getItem('userLocalStorage'));
    const result = _.has(localStorageItem, key);
    expect(result).toBeFalsy();
  }));
  it('should return throw error on object removal', inject([LocalUserStorageService],
    async(localStorageService: LocalUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await localStorageService.setItem(key, value);
    localStorageService.removeItem('key1').then(() => {}).catch(err => {
      expect(err).toEqual('Key was not found in the object');
    });

  }));
});

describe('SessionUserStorageService', () => {
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        ApiTestingModule.forRoot(),
        CommonModule,
        FormsModule,
        MostModule.forRoot({
          base: '/api/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [
      ],
      providers: [
        SessionUserStorageService
      ]
    }).compileComponents();
  });

  afterAll(async() => {
    sessionStorage.clear();
  });

  it('should initiate a new service', inject([AngularDataContext], (context: AngularDataContext) => {
    const sessionStorageService = new SessionUserStorageService(context);
    expect(sessionStorageService).toBeTruthy();
  }));

  it('should get item from sessionStorage', inject( [SessionUserStorageService],
    async(sessionStorageService: SessionUserStorageService) => {
    expect(await sessionStorageService.getItem('key')).toEqual({key:'key', value:null});
  }));

  it('should set item to sessionStorage', inject( [SessionUserStorageService],
    async(sessionStorageService: SessionUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await sessionStorageService.setItem(key, value);
    const sessionStorageItem = JSON.parse(sessionStorage.getItem('userSessionStorage'));
    const result = _.has(sessionStorageItem, key);
    const expectedValue = _.get(sessionStorageItem, key);
    expect(result).toBe(true);
    expect(expectedValue).toEqual(value);
  }));
  it('should remove an item from sessionStorage', inject([SessionUserStorageService],
    async(sessionStorageService: SessionUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await sessionStorageService.setItem(key, value);
    await sessionStorageService.removeItem(key);
    const sessionStorageItem = JSON.parse(sessionStorage.getItem('userSessionStorage'));
    const result = _.has(sessionStorageItem, key);
    expect(result).toBeFalsy();
  }));
  it('should return throw error on object removal from session storage', inject([SessionUserStorageService],
    async(sessionStorageService: SessionUserStorageService) => {
    const key = 'key';
    const value = 'value';
    await sessionStorageService.setItem(key, value);
    sessionStorageService.removeItem('key1').then(() => {}).catch(err => {
      expect(err).toEqual('Key was not found in the object');
    });

  }));
});

