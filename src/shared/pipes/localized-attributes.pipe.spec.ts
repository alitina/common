import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { LocalizedAttributesPipe } from './localized-attributes.pipe';
import {ConfigurationService} from '../services/configuration.service';
import {CommonModule} from '@angular/common';
import {TestingConfigurationService} from '../../../testing/src/testing-configuration.service';

// test data
const  course_en = {
    "name": "THEORY OF ELASTICITY",
    "locale": {
        "name": "THEORY OF ELASTICITY",
        "inLanguage": "en"
    }}
const  course_gr = {
    "name": "ΘΕΩΡΙΑ ΕΛΑΣΤΙΚΟΤΗΤΑΣ",
    "locale": {
        "name": "ΘΕΩΡΙΑ ΕΛΑΣΤΙΚΟΤΗΤΑΣ",
        "inLanguage": "el"
    }}

const  course_mix = {
    "name": "ΘΕΩΡΙΑ ΕΛΑΣΤΙΚΟΤΗΤΑΣ",
    "locale": {
        "name": "THEORY OF ELASTICITY",
        "inLanguage": "en"
    }}

const  course_no_locale = {
    "name": "ΘΕΩΡΙΑ ΕΛΑΣΤΙΚΟΤΗΤΑΣ"
    }

const  course_undefined = {

    }


describe('LocalizedAttributesPipe', () => {
    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            imports: [
                CommonModule,
                TranslateModule.forRoot()
            ],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }
            ]
        }).compileComponents();
        }));

        it('should create', () => {
            expect(LocalizedAttributesPipe).toBeTruthy();
          });

        it('should display EN translation', inject([TranslateService],
            (translateService: TranslateService) => {
                translateService.currentLang = 'en';
                const localizedPipe = new LocalizedAttributesPipe(translateService);
            expect(localizedPipe.transform(course_en, 'name')).toBe('THEORY OF ELASTICITY')
          }));

          it('should display GR translation', inject([TranslateService],
            (translateService: TranslateService) => {
                translateService.currentLang = 'gr';
                const localizedPipe = new LocalizedAttributesPipe(translateService);
            expect(localizedPipe.transform(course_gr, 'name')).toBe('ΘΕΩΡΙΑ ΕΛΑΣΤΙΚΟΤΗΤΑΣ')
          }));

          it('should display EN translation when data is mixed and current language EN', inject([TranslateService],
            (translateService: TranslateService) => {
                translateService.currentLang = 'en';
                const localizedPipe = new LocalizedAttributesPipe(translateService);
            expect(localizedPipe.transform(course_mix, 'name')).toBe('THEORY OF ELASTICITY')
          }));

          it('should display GR translationwhen data is mixed and current language GR', inject([TranslateService],
            (translateService: TranslateService) => {
                translateService.currentLang = 'el';
                const localizedPipe = new LocalizedAttributesPipe(translateService);
            expect(localizedPipe.transform(course_mix, 'name')).toBe('ΘΕΩΡΙΑ ΕΛΑΣΤΙΚΟΤΗΤΑΣ')
          }));

          it('should display name when there is no locale', inject([TranslateService],
            (translateService: TranslateService) => {
                translateService.currentLang = 'en';
                const localizedPipe = new LocalizedAttributesPipe(translateService);
            expect(localizedPipe.transform(course_no_locale, 'name')).toBe('ΘΕΩΡΙΑ ΕΛΑΣΤΙΚΟΤΗΤΑΣ')
          }));

          it('should return undefined if there is no data in attribute', inject([TranslateService],
            (translateService: TranslateService) => {
                translateService.currentLang = 'en';
                const localizedPipe = new LocalizedAttributesPipe(translateService);
            expect(localizedPipe.transform(course_undefined, 'name')).toBe(undefined)
          }));
});
